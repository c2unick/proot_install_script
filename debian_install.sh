#!/data/data/com.termux/files/usr/bin/bash
cd "$HOME"
tarball="debian-rootfs.tar.xz"
#wget "https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Rootfs/Debian/${archurl}/debian-rootfs-${archurl}.tar.xz" -O $tarball
folder=$HOME/.local/share/proot-containers/debian
mkdir -p "$folder"
cd "$folder"
mkdir -p "debian-rootfs"
mkdir -p "debian-binds"
echo "uncompressing...."
cd "debian-rootfs"
proot --link2symlink tar -xJf ~/${tarball}||:
cd "$folder"
bin=start-debian
echo "writing launch script"
cat > $bin <<- EOM
#!/bin/bash
cd \$(dirname \$0)
## unset LD_PRELOAD in case termux-exec is installed
unset LD_PRELOAD
command="proot"
command+=" --link2symlink"
command+=" -0"
command+=" -r $folder/debian-rootfs"
if [ -n "\$(ls -A $folder/debian-binds)" ]; then
    for f in $folder/debian-binds/* ;do
      . \$f
    done
fi
command+=" -b /dev"
command+=" -b /proc"
command+=" -b $folder/debian-rootfs/root:/dev/shm"
## uncomment the following line to have access to the home directory of termux
#command+=" -b /data/data/com.termux/files/home:/root"
## uncomment the following line to mount /sdcard directly to / 
#command+=" -b /sdcard"
command+=" -w /root"
command+=" /usr/bin/env -i"
command+=" HOME=/root"
command+=" PATH=/usr/local/sbin:/usr/local/bin:/bin:/usr/bin:/sbin:/usr/sbin:/usr/games:/usr/local/games"
command+=" TERM=\$TERM"
command+=" LANG=C.UTF-8"
command+=" /bin/bash --login"
#echo $command
com="\$@"
if [ -z "\$1" ];then
    exec \$command
else
    \$command -c "\$com"
fi
EOM
termux-fix-shebang $bin
chmod +x $bin
cd "debian-rootfs/etc"
rm resolv.conf && echo >>resolv.conf nameserver 8.8.8.8
ln -s ~/.local/share/proot-containers/debian/start-debian /data/data/com.termux/files/usr/bin/startdebian
echo "done"